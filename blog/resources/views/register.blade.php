<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">

            @csrf

            <p>First name:</p>
            <input type="text" name="fname" />
            <p>Last name:</p>
            <input type="text" name="lname" />
            <p>Gender:</p>
            <input type="radio" name="gender" value="male" /> Male
            <br />
            <input type="radio" name="gender" value="female" /> Female
            <br />
            <input type="radio" name="gender" value="other" /> Other
            <br />
            <p>Nationality:</p>
            <select name="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="malaysian">Malaysian</option>
                <option value="korean">Korean</option>
            </select>
            <p>Language Spoken:</p>
                <input type="checkbox" name="indonesia" value="indonesia" /> Bahasa Indonesia
                <br />
                <input type="checkbox" name="english" value="english" /> English
                <br />
                <input type="checkbox" name="other" value="other" /> Other
                <br />
            <p>Bio:</p>
                <textarea name="bio" rows="8" cols="30"></textarea>
                <br />
            <input type="submit" value="Sign Up" />
        </form> 
</body>
</html>