<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        $fnama = $request->fname;
        $lnama = $request->lname;
        $gender = $request->gender;
        $nationality = $request->nationality;
        $indonesia = $request->indonesia;
        $english = $request->english;
        $other = $request->other;
        $bio = $request->bio;

        return view('welcome', compact('fnama', 'lnama'));
    }
    
}